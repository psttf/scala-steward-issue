libraryDependencies ++= Seq(
  // we only support Boostrap 3 at the moment so please do not update it
  "org.webjars.npm" % "bootstrap" % "3.4.1", // scala-steward:off
  "org.webjars.npm" % "jquery" % "4.3.1",
)
